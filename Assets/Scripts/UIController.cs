﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    #region Public References
    public Transform songsDropdownList;
    public GameObject songDropdownItem;
    public TextMeshProUGUI songTitleText;
    public TextMeshProUGUI elapsedTime;
    public TextMeshProUGUI totalTime;
    #endregion

    #region Properties
    private MusicOutputManager _instance;
    private AudioSource Source => _instance.Source;
    private AudioClip CurrentSong => _instance.CurrentSong;
    #endregion
    

    private void Start()
    {
        _instance = MusicOutputManager.Instance;
        _instance.NewSongPlayed += () => songTitleText.text = CurrentSong.name;
        InstantiateSongListUI();
    }

    private void Update()
    {
        UpdateTrackTime();
    }

    private void UpdateTrackTime()
    {
        if (Source == null || CurrentSong == null)
            return;

        float elapsed = Source.time;
        float total = CurrentSong.length;

        var elapsedTimeSpan = TimeSpan.FromSeconds(elapsed);
        var totalTimeSpan = TimeSpan.FromSeconds(total);

        elapsedTime.text = elapsedTimeSpan.ToString("mm\\:ss");
        totalTime.text = totalTimeSpan.ToString("mm\\:ss");
    }
    
    private void InstantiateSongListUI()
    {
        foreach (var song in _instance.Songs)
        {
            var item = Instantiate(songDropdownItem, songsDropdownList);

            var button = item.GetComponent<Button>();
            var title = item.GetComponentInChildren<TextMeshProUGUI>();

            title.text = song.Value.name;
            button.onClick.AddListener(delegate { _instance.SwitchToSong(song.Key); });
        }
    }
}
