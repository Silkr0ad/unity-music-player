﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class HandleController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public AudioSource source;

    private Slider _slider;
    private bool _shouldFollowMusic = true;

    private void Awake()
    {
        _slider = GetComponent<Slider>();
    }

    private void Start()
    {
        MusicOutputManager.Instance.NewSongPlayed += ResetProgressBar;
        MusicOutputManager.Instance.NewSongPlayed += () => _slider.interactable = true;
    }

    private void Update()
    {
        if (_shouldFollowMusic)
            FollowMusic();
    }

    private void FollowMusic()
    {
        if (source == null || source.clip == null)
            return;

        var clip = source.clip;
        float ratio = source.time / clip.length;
        _slider.value = ratio;
    }

    // This function is called by OnValueChange on the Slider component, subscribed in the Inspector.
    public void ControlClipProgress()
    {
        if (source == null || source.clip == null)
            return;
        
        float ratio = _slider.value;
        float clipTime = source.clip.length * ratio;
        source.time = clipTime;
        source.UnPause();
    }

    private void ResetProgressBar()
    {
        _slider.value = 0f;
        _shouldFollowMusic = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _shouldFollowMusic = true;
        source.UnPause();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _shouldFollowMusic = false;
        source.Pause();
    }
}
