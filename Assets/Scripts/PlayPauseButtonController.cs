﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayPauseButtonController : MonoBehaviour
{
    public Image targetImage;
    public Sprite playSprite;
    public Sprite pauseSprite;
    
    private void Start()
    {
        MusicOutputManager instance = MusicOutputManager.Instance;
        
        instance.NewSongPlayed += () => targetImage.sprite = pauseSprite;
        instance.SongUnpaused += () => targetImage.sprite = pauseSprite;
        instance.SongPaused += () => targetImage.sprite = playSprite;
    }
}
