﻿using System;
using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class MusicOutputManager : MonoBehaviour
{
    public static MusicOutputManager Instance;
    
    [Tooltip("Directory under Resources that contains the songs. E.g.: 'Sample Songs'.")]
    public string songsDirectory;

    public Dictionary<int, AudioClip> Songs { get; private set; }
    public AudioClip CurrentSong { get; private set; }
    public AudioSource Source { get; private set; }

    #region Events
    public event Action NewSongPlayed;
    public event Action SongPaused;
    public event Action SongUnpaused;
    #endregion
    
    private void Awake()
    {
        // Create the singleton.
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
        
        // Initialize private references.
        Source = GetComponent<AudioSource>();
        
        InitializeSongsDictionary();
    }

    private void InitializeSongsDictionary()
    {
        Songs = new Dictionary<int, AudioClip>();
        var songArray = Resources.LoadAll<AudioClip>(songsDirectory);

        for (int i = 0; i < songArray.Length; i++)
            Songs.Add(i, songArray[i]);
    }

    public void SwitchToSong(int songKey)
    {
        CurrentSong = Songs[songKey];
        Source.clip = CurrentSong;
        Source.Play();
        NewSongPlayed?.Invoke();
    }

    public void TogglePause()
    {
        if (Source == null || CurrentSong == null)
            return;
        
        if (Source.isPlaying)
        {
            Source.Pause();
            SongPaused?.Invoke();
        }
        else
        {
            Source.UnPause();
            SongUnpaused?.Invoke();
        }
    }
}
